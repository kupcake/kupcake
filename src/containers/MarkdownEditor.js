import { connect } from 'react-redux';

import { saveDocument,
         openDocument,
         openDocumentFromContent,
         closeDocument,
         changeDocument,
         updateDocument,
         newDocument } from '../document';

import { MarkdownEditor as Component } from '../components/MarkdownEditor';

let mapStateToProps = (state) =>
{
  return {
    documents: state.document,
  };
}

let mapDispatchToProps = (dispatch) =>
{
  return {
    saveDocument: (document_path, editor_state) => dispatch(saveDocument(document_path, editor_state)),
    openDocument: () => dispatch(openDocument()),
    openDocumentFromContent: (name, document_content) => dispatch(openDocumentFromContent(name, document_content)),
    newDocument: () => dispatch(newDocument()),
    updateDocument: (document_path, editor_state) => dispatch(updateDocument(document_path, editor_state)),
    closeDocument: (document_path, editor_state) => dispatch(closeDocument(document_path, editor_state)),
    changeDocument: (document_path) => dispatch(changeDocument(document_path)),
  };
}

export let MarkdownEditor = connect(mapStateToProps, mapDispatchToProps)(Component);
