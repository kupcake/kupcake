import * as actionTypes from './actionTypes';

import { convertToRaw } from 'draft-js';
import { draftToMarkdown } from 'markdown-draft-js';
import { openFile, saveFile, save } from '../binding';

/**
 *
 */
export let openDocument = () =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.OPEN_DOCUMENT});
    let response = openFile();
    if (response.error)
    {
      dispatch({type: actionTypes.OPEN_DOCUMENT_FAILURE});
    }
    else
    {
      dispatch({type: actionTypes.OPEN_DOCUMENT_SUCCESS, document_path: response.document_path, document_content: response.document_content});
    }
  }
}

/**
 *
 */
export let openDocumentFromContent = (name, document_content) =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.OPEN_DOCUMENT_FROM_CONTENT, name: name, document_content: document_content});
  }
}

/**
 *
 */
export let updateDocument = (document_path, editor_state) =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.UPDATE_DOCUMENT, document_path: document_path, editor_state: editor_state});
  }
}

/**
 *
 */
export let saveDocument = (document_path, editor_state) =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.SAVE_DOCUMENT});
    let raw = convertToRaw(editor_state.getCurrentContent());
    let markdown = draftToMarkdown(raw);
    let response = saveFile(document_path, markdown);
    if (response.error)
    {
      dispatch({type: actionTypes.SAVE_DOCUMENT_FAILURE});
    }
    else
    {
      dispatch({
        type: actionTypes.SAVE_DOCUMENT_SUCCESS,
        old_document_path: document_path,
        document_path: response.document_path,
        document_content: response.document_content
      });
    }
  }
}

/**
 *
 */
export let newDocument = () =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.NEW_DOCUMENT});
  }
}

/**
 *
 */
export let closeDocument = (document_path, editor_state) =>
{
  return async (dispatch, getState) =>
  {
    let documents = getState().document;
    let changed = documents[document_path].changed;

    if (changed)
    {
      let result = save();
      if (result.save && !result.cancel)
      {
        await dispatch(await saveDocument(document_path, editor_state));
        dispatch({type: actionTypes.CLOSE_DOCUMENT, document_path: document_path});
      }
      else if (!result.save && !result.cancel)
      {
        dispatch({type: actionTypes.CLOSE_DOCUMENT, document_path: document_path});
      }
    }
    else
    {
      dispatch({type: actionTypes.CLOSE_DOCUMENT, document_path: document_path});
    }
  }
}

/**
 *
 */
export let changeDocument = (document_path) =>
{
  return async dispatch =>
  {
    dispatch({type: actionTypes.CHANGE_DOCUMENT, document_path: document_path});
  }
}
