import * as actionTypes from './actionTypes';

import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import { draftToMarkdown, markdownToDraft } from 'markdown-draft-js';


let createUniqueId = () =>
{
  return new Date().getUTCMilliseconds();
}

let createDocumentFromContent = (name, document_content) =>
{
  let raw_object = markdownToDraft(document_content);
  let content_state = convertFromRaw(raw_object);
  let editor_state = EditorState.createWithContent(content_state);

  return {
    name: name,
    editor_state: editor_state,
    saved: '',
    selected: true,
    untitled: false,
    changed: false,
  }
}

let createEmptyDocument = () =>
{
  return {
    name: 'untitled',
    editor_state: EditorState.createEmpty(),
    saved: '',
    selected: true,
    untitled: true,
    changed: false,
  }
}

export let reducer = (
  state = {
    [createUniqueId()]: createEmptyDocument(),
  },
  action) =>
{
  switch(action.type)
  {
    case actionTypes.OPEN_DOCUMENT:
      return state;

    case actionTypes.OPEN_DOCUMENT_SUCCESS:
    {
      for (let key in state) state[key].selected = false;
      return {
        ...state,
        [action.document_path]: createDocumentFromContent(action.document_path.split('\\').pop().split('/').pop(), action.document_content)
      };
    }

    case actionTypes.OPEN_DOCUMENT_FROM_CONTENT:
      return {
        ...state,
        [createUniqueId()]: createDocumentFromContent(action.name, action.document_content)
      };

    case actionTypes.OPEN_DOCUMENT_FAILURE:
      console.log('OPEN_DOCUMENT_FAILURE');
      return state;

    case actionTypes.SAVE_DOCUMENT:
      return state;

    case actionTypes.SAVE_DOCUMENT_SUCCESS:
    {
      let raw_object = markdownToDraft(action.document_content);
      let content_state = convertFromRaw(raw_object);
      let editor_state = EditorState.createWithContent(content_state);

      return {
        ...state,
        [action.document_path]: {
          ...state[action.document_path],
          editor_state: editor_state,
          saved: action.document_content,
          changed: false,
        }
      };
    }

    case actionTypes.SAVE_DOCUMENT_FAILURE:
      console.log('SAVE_DOCUMENT_FAILURE');
      return state;

    case actionTypes.UPDATE_DOCUMENT:
    {
      let new_raw = convertToRaw(action.editor_state.getCurrentContent());
      let new_markdown = draftToMarkdown(new_raw);
      let changed = new_markdown !== state[action.document_path].saved;

      return {
        ...state,
        [action.document_path]: {
          ...state[action.document_path],
          editor_state: action.editor_state,
          changed: changed,
        }
      };
    }

    case actionTypes.NEW_DOCUMENT:
      for (let key in state) state[key].selected = false;
      return {
        ...state,
        [createUniqueId()]: createEmptyDocument(),
      };

    case actionTypes.CLOSE_DOCUMENT:
    {
      let is_selected = state[action.document_path].selected;
      let index = Object.keys(state).indexOf(action.document_path);

      if (is_selected)
      {
        for (let key in state) state[key].selected = false;
      }
      delete state[action.document_path];
      let new_document_paths = Object.keys(state);

      if (new_document_paths.length === 0)
      {
        return {
          [createUniqueId()]: createEmptyDocument(),
        }
      }
      else
      {
        if (is_selected)
        {
          if (index === new_document_paths.length)
          {
            index = new_document_paths.length - 1;
          }
          return {
            ...state,
            [new_document_paths[index]]: {
              ...state[new_document_paths[index]],
              selected: true,
            }
          };
        }
        else
        {
          return {
            ...state
          }
        }
      }
    }

    case actionTypes.CHANGE_DOCUMENT:
      for (let key in state) state[key].selected = false;
      return {
        ...state,
        [action.document_path]: {
          ...state[action.document_path],
          selected: true,
        }
      };

    default:
      return state;
  }
}
