import React from 'react';
// import Autolinker from 'autolinker';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

import { store, history } from './store';
import { MarkdownEditor } from './containers/MarkdownEditor';

export default class App extends React.Component
{
  render()
  {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path="/" component={MarkdownEditor}/>
          </Switch>
        </ConnectedRouter>
      </Provider>
    )
  }
}
