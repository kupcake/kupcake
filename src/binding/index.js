export { openInBrowser,
         setShortcut,
         openFile,
         save,
         saveFile,
         openWindow,
         closeWindow,
         closeAllWindows } from './electron.js'
