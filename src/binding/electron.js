const { ipcRenderer, shell, remote } = window.require('electron');
const localshortcut = remote.require('electron-localshortcut');

/**
 *
 */
export let openInBrowser = (url) =>
{
  shell.openExternal(url);
}

/**
 *
 */
export let setShortcut = (shortcut, callback) =>
{
  if (shortcut && callback)
  {
    const current_window = remote.getCurrentWindow();
    localshortcut.register(current_window, shortcut, callback);
  }
}

/**
 *
 */
export let openFile = () =>
{
  return ipcRenderer.sendSync('openFile');
}

/**
 *
 */
export let save = () =>
{
  return ipcRenderer.sendSync('save');
}

/**
 *
 */
export let saveFile = (path, content) =>
{
  let data = {
    document_content: content,
    document_path: path,
  }
  return ipcRenderer.sendSync('saveFile', data);
}

/**
 *
 */
export let openWindow = () =>
{
  ipcRenderer.send('openWindow');
}

/**
 *
 */
export let closeWindow = () =>
{
  const current_window = remote.getCurrentWindow();
  current_window.close();
}

/**
 *
 */
export let closeAllWindows = () =>
{
  const { BrowserWindow } = remote;
  const current_windows = BrowserWindow.getAllWindows();
  for (const index in current_windows)
  {
    let current_window = current_windows[index];
    current_window.close();
  }
}
