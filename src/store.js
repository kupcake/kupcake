import createHistory from 'history/createHashHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { documentReducer } from './document';

const rootReducer = combineReducers({
  route: routerReducer,
  document: documentReducer,
});

export const history = createHistory()
const router_middleware = routerMiddleware(history)
export const store = createStore(rootReducer, applyMiddleware(router_middleware, thunk));
