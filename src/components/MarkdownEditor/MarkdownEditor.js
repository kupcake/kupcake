import React from 'react';
import { Editor, RichUtils, getDefaultKeyBinding } from 'draft-js';

import './styles.css';

const styleMap = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
};

export class MarkdownEditorEmbed extends React.Component
{
  /**
   *
   */
  constructor(props)
  {
    super(props);
    this.focus = () => this.refs.editor.focus();
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this.mapKeyToEditorCommand = this.mapKeyToEditorCommand.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  /**
   *
   */
  onChange(new_state)
  {
    this.props.updateDocument(this.props.document_path, new_state);
  }

  /**
   *
   */
  handleKeyCommand(command, editorState)
  {
    const new_state = RichUtils.handleKeyCommand(editorState, command);

    if (new_state)
    {
      this.props.updateDocument(this.props.document_path, new_state);
      return true;
    }
    return false;
  }

  /**
   *
   */
  mapKeyToEditorCommand(e)
  {
    if (e.keyCode === 9)
    {
      const new_state = RichUtils.onTab(
        e,
        this.props.editor_state,
        4,
      );
      if (new_state !== this.props.editor_state)
      {
        this.props.updateDocument(this.props.document_path, new_state);
      }
      return;
    }
    return getDefaultKeyBinding(e);
  }

  /**
   *
   */
  getBlockStyle(block)
  {
    switch (block.getType())
    {
      case 'blockquote': return 'RichEditor-blockquote';
      default: return null;
    }
  }

  /**
   *
   */
  render()
  {
    let className = 'markdown-editor markdown-body';
    var content_state = this.props.editor_state.getCurrentContent();
    if (!content_state.hasText())
    {
      if (content_state.getBlockMap().first().getType() !== 'unstyled')
      {
        className += ' RichEditor-hidePlaceholder';
      }
    }

    return (
      <div>
        <div className={className} onClick={this.focus}>
          <Editor
            blockStyleFn={this.getBlockStyle}
            customStyleMap={styleMap}
            editorState={this.props.editor_state}
            handleKeyCommand={this.handleKeyCommand}
            keyBindingFn={this.mapKeyToEditorCommand}
            onChange={this.onChange}
            placeholder=''
            ref="editor"
            spellCheck={true}/>
        </div>
      </div>
    );
  }
}
