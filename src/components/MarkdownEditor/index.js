import React from 'react';
import { RichUtils } from 'draft-js';
import { Helmet } from 'react-helmet';

import './styles.css';

import { Menu, Item, Separator, Dropdown } from '../Menu';
import { MarkdownEditorEmbed } from './MarkdownEditor';

import { version } from '../../info';
import { openInBrowser, openWindow, closeWindow, closeAllWindows } from '../../binding';

const BLOCK_TYPES = [
  {label: 'H1', style: 'header-one'},
  {label: 'H2', style: 'header-two'},
  {label: 'H3', style: 'header-three'},
  {label: 'H4', style: 'header-four'},
  {label: 'H5', style: 'header-five'},
  {label: 'H6', style: 'header-six'},
];

const OTHER_TYPES = [
  {label: 'Blockquote', style: 'blockquote', icon: 'fa fa-quote-right'},
  {label: 'Code Block', style: 'code-block', icon: 'fa fa-code'},
];

const LIST_TYPES = [
  {label: 'ul', style: 'unordered-list-item', icon: 'fa fa-list-ul'},
  {label: 'ol', style: 'ordered-list-item', icon: 'fa fa-list-ol'},
];

var INLINE_STYLES = [
  {label: 'bold', style: 'BOLD', icon: 'fa fa-bold'},
  {label: 'italic', style: 'ITALIC', icon: 'fa fa-italic'},
  {label: 'Monospace', style: 'CODE', icon: 'fa fa-code'},
];

/**
 *
 */
export class MarkdownEditor extends React.Component
{
  /**
   *
   */
  constructor(props)
  {
    super(props);

    this.toggleBlockType = this.toggleBlockType.bind(this);
    this.toggleInlineStyle = this.toggleInlineStyle.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  /**
   *
   */
  getCurrentDocumentPath()
  {
    let { documents } = this.props;
    let current_document_path = '';

    for (let document_path in documents)
    {
      let document = documents[document_path];
      if (document.selected)
      {
        current_document_path = document_path;
      }
    }

    return current_document_path;
  }

  /**
   *
   */
  toggleBlockType(blockType)
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const editor_state = documents[document_path].editor_state;

    this.props.updateDocument(
      document_path,
      RichUtils.toggleBlockType(
        editor_state,
        blockType
      )
    );
  }

  /**
   *
   */
  toggleInlineStyle(inlineStyle)
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const editor_state = documents[document_path].editor_state;

    this.props.updateDocument(
      document_path,
      RichUtils.toggleInlineStyle(
        editor_state,
        inlineStyle
      )
    );
  }

  handleSave()
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const document = documents[document_path];
    if (document.untitled)
    {
      this.props.saveDocument(null, document.editor_state);
    }
    else
    {
      this.props.saveDocument(document_path, document.editor_state);
    }
  }

  handleClose()
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const document = documents[document_path];
    this.props.closeDocument(document_path, document.editor_state);
  }

  getBlockStyleControles(types)
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const editor_state = documents[document_path].editor_state;
    const selection = editor_state.getSelection();
    const blockType = editor_state
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();

    let components = types.map(type =>
    {
      let content = (
        <span>{type.label}</span>
      )

      if (type.icon)
      {
        content = (
          <i className={type.icon} aria-hidden="true"></i>
        )
      }

      return (
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          label={type.label}
          onToggle={this.toggleBlockType}
          style={type.style}>
          {content}
        </StyleButton>
      )
    });

    return components;
  }

  getInlineStyleControles()
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const editor_state = documents[document_path].editor_state;
    const currentStyle = editor_state.getCurrentInlineStyle();

    let components = INLINE_STYLES.map((type) =>
      <StyleButton
        key={type.label}
        active={currentStyle.has(type.style)}
        label={type.label}
        onToggle={this.toggleInlineStyle}
        style={type.style}>
        <i className={type.icon} aria-hidden="true"></i>
      </StyleButton>
    )

    return components;
  }

  getTabs()
  {
    let { documents } = this.props;

    let components = Object.keys(documents).map((document_path, index) =>
    {
      let document = documents[document_path];
      let className = document.selected ? 'is-active' : '';
      let icon = (
        <i className="far fa-times-circle" aria-hidden="true"></i>
      )
      if (document.changed)
      {
        icon = (
          <i className="far fa-dot-circle" aria-hidden="true"></i>
        )
      }
      return (
        <li key={document_path} className={className}>
          <a>
            <span onClick={() => this.props.changeDocument(document_path)}>{document.name}</span>
            <span onClick={() => this.props.closeDocument(document_path, document.editor_state)} className="icon is-small">
              {icon}
            </span>
          </a>
        </li>
      )
    });

    return components;
  }

  render()
  {
    let { documents } = this.props;
    const document_path = this.getCurrentDocumentPath();
    const editor_state = documents[document_path].editor_state;

    return (
      <div>

        <Helmet>
          <title>{documents[document_path].name}{' - Kupcake'}</title>
        </Helmet>

        <div className="tabs is-boxed">
          <ul>
            {this.getTabs()}
          </ul>
        </div>

        <Menu>
          <Dropdown title='File'>
            <Item onClick={openWindow} shortcut={'Ctrl+Shift+N'}>{'New window'}</Item>
            <Item onClick={this.props.newDocument} shortcut={'Ctrl+N'}>{'New document'}</Item>
            <Item onClick={this.props.openDocument} shortcut={'Ctrl+O'}>{'Open document...'}</Item>
            <Separator/>
            <Item onClick={this.handleSave} shortcut={'Ctrl+S'}>{'Save'}</Item>
            <Item onClick={this.handleSaveAs} shortcut={'Ctrl+Shift+S'}>{'Save as...'}</Item>
            <Item onClick={this.handleSaveAll}>{'Save all'}</Item>
            <Separator/>
            <Item onClick={this.handleClose} shortcut={'Ctrl+W'}>{'Close document'}</Item>
            <Item onClick={closeWindow} shortcut={'Ctrl+Shift+W'}>{'Close window'}</Item>
            <Separator/>
            <Item onClick={closeAllWindows} shortcut={'Ctrl+Q'}>{'Quit'}</Item>
          </Dropdown>
          <Dropdown title='Edit'>
            <Item shortcut={'Ctrl+Z'}>{'Undo'}</Item>
            <Item shortcut={'Ctrl+Shift+Z'}>{'Redo'}</Item>
            <Separator/>
            <Item shortcut={'Ctrl+X'}>{'Cut'}</Item>
            <Item shortcut={'Ctrl+C'}>{'Copy'}</Item>
            <Item shortcut={'Ctrl+V'}>{'Paste'}</Item>
            <Item shortcut={'Ctrl+A'}>{'Select all'}</Item>
            <Separator/>
            <Item shortcut={'Ctrl+F'}>{'Find and replace...'}</Item>

          </Dropdown>
          <Dropdown title='Insert'>
            <Item>{'Table'}</Item>
            <Item>{'Image'}</Item>
            <Item>{'Horizontal Line'}</Item>
            <Item>{'Footnote'}</Item>
          </Dropdown>
          <Dropdown title='Help'>
            <Item>{'View licence'}</Item>
            <Item>{'Version '}{version}</Item>
            <Separator/>
            <Item onClick={() => openInBrowser('https://gitlab.com/kupcake/kupcake/issues/new')}>{'Report issue'}</Item>
            <Item onClick={() => openInBrowser('https://gitlab.com/kupcake/kupcake/issues')}>{'Search issues'}</Item>
            <Separator/>
            <Item onClick={() => openInBrowser('https://kupcake.gitlab.io')}>{'About Kupcake'}</Item>
          </Dropdown>
        </Menu>

        <nav className="navbar is-light" aria-label="main navigation">
          <div className="navbar-menu">
            {this.getBlockStyleControles(BLOCK_TYPES)}
            <div className="navbar-separator"/>
            {this.getBlockStyleControles(OTHER_TYPES)}
            <div className="navbar-separator"/>
            {this.getBlockStyleControles(LIST_TYPES)}
            <div className="navbar-separator"/>
            {this.getInlineStyleControles()}
            <div className="navbar-separator"/>
          </div>
        </nav>

        <MarkdownEditorEmbed
          editor_state={editor_state}
          document_path={document_path}
          updateDocument={this.props.updateDocument}
          ref={this.editor}/>

      </div>
    );
  }
}

class StyleButton extends React.Component
{
  constructor()
  {
    super();
    this.onToggle = (e) =>
    {
      e.preventDefault();
      this.props.onToggle(this.props.style);
    };
  }

  render()
  {
    let className = 'navbar-item';
    if (this.props.active)
    {
      className += ' is-active';
    }

    return (
      <a className={className} onMouseDown={this.onToggle}>
        {this.props.children}
      </a>
    );
  }
}
