import React from 'react';

import './styles.css';

export class Menu extends React.Component
{
  render()
  {
    return (
      <nav className="menu navbar is-light" aria-label="main navigation">
        <div className="navbar-menu">
          {this.props.children}
        </div>
      </nav>
    )
  }
}
