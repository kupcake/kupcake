import React from 'react';

import { setShortcut } from '../../binding';

export class Item extends React.Component
{
  componentDidMount()
  {
    setShortcut(this.props.shortcut, this.props.onClick);
  }

  render()
  {
    let className = 'navbar-item';
    if (this.props.onClick === undefined)
    {
      className += ' is-disabled'
    }
    return (
      <a className={className} onClick={this.props.onClick}>
        <div className={'action'}>{this.props.children}</div>
        <div className={'shortcut'}>{this.props.shortcut}</div>
      </a>
    )
  }
}
