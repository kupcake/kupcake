const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const fs = require('fs');
const path = require('path');
const url = require('url');
const isDev = require('electron-is-dev');
const { ipcMain } = require('electron')

let mainWindow;

/**
 *
 */
var createWindow = () =>
{
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 680,
    minHeight: 360,
    minWidth: 1024,
    icon: path.join(__dirname, 'icons/64x64.png'),
  });
  mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
  mainWindow.on('closed', () => mainWindow = null);
  // mainWindow.setMenu(null);
}

/**
 *
 */
app.on('ready', createWindow);

/**
 *
 */
app.on('window-all-closed', () =>
{
  if (process.platform !== 'darwin')
  {
    app.quit();
  }
});

/**
 *
 */
app.on('activate', () =>
{
  if (mainWindow === null)
  {
    createWindow();
  }
});

/**
 *
 */
ipcMain.on('openWindow', (event) =>
{
  createWindow();
});

/**
 *
 */
ipcMain.on('openFile', (event, data) =>
{
  showOpenDialog(event, data);
});

/**
 *
 */
ipcMain.on('saveFile', (event, data) =>
{
  showSaveDialog(event, data);
});

/**
 *
 */
ipcMain.on('save', (event) =>
{
  showSavePrompt(event);
});

/**
 *
 */
var showOpenDialog = (event, data) =>
{
  const { dialog } = require('electron');
  const fs = require('fs');

  var filters = [
    {name: 'Markdown', extensions: ['md']},
    {name: 'All Files', extensions: ['*']},
  ];

  var options = {
    filters: filters,
  };

  dialog.showOpenDialog(options, (file_names) =>
  {
    let response = {
      error: false,
      document_path: '',
      data: '',
    };

    if (file_names === undefined)
    {
      response.error = true;
      event.returnValue = response;
    }
    else
    {
      let document_path = file_names[0];
      response.document_path = document_path;

      fs.readFile(document_path, 'utf-8', (err, data) =>
      {
        if (err)
        {
          response.error = true;
          event.returnValue = response;
        }
        else
        {
          response.document_content = data;
          event.returnValue = response;
        }
      });
    }
  });
}

/**
 *
 */
var showSaveDialog = (event, data) =>
{
  const { dialog } = require('electron');
  const fs = require('fs');

  let response = {
    error: false,
    document_path: '',
    document_content: '',
  };

  if (data.document_path == null)
  {
    dialog.showSaveDialog({}, document_path =>
    {
      if (document_path)
      {
        fs.writeFile(document_path, data.document_content, (err) =>
        {
          if (err)
          {
            response.error = true;
            event.returnValue = response;
          }
          else
          {
            response.document_content = data.document_content;
            response.document_path = document_path;
            event.returnValue = response;
          }
        });
      }
      else
      {
        response.error = true;
        event.returnValue = response;
      }
    });
  }
  else
  {
    fs.writeFile(data.document_path, data.document_content, (err) =>
    {
      if (err)
      {
        response.error = true;
        event.returnValue = response;
      }
      else
      {
        response.document_content = data.document_content;
        response.document_path = data.document_path;
        event.returnValue = response;
      }
    });
  }
}

/**
 *
 */
var showSavePrompt = (event, data) =>
{
  const { dialog } = require('electron');

  let options = {
    type: 'question',
    buttons: ['Save', 'Cancel', 'Don\'t save'],
    title: '\'untitled\' has changes, do you want to save them?',
    message: 'Your changes will be lost if you close this item without saving.',
  }

  dialog.showMessageBox(options, data =>
  {
    let response = {
      save: false,
      cancel: false,
    };

    if (data == 0)
    {
      response.save = true;
    }
    else if (data == 1)
    {
      response.cancel = true;
    }

    event.returnValue = response;
  });
}
